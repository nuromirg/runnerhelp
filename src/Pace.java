import java.util.Scanner;

public class Pace {
    String pace;
    Scanner scanner = new Scanner(System.in);
    int[] intArray = new int[2];
    int seconds;

    int parse(){
        pace = scanner.nextLine();
        String[]  arrayTemp = String.valueOf(pace).split(":");
        intArray[0] = Integer.parseInt(arrayTemp[0]);
        intArray[1] = Integer.parseInt(arrayTemp[1]);
        seconds = intArray[0] * 60 + intArray[1];
        return seconds;
    }

}
