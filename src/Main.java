import java.util.Scanner;

public class Main {

	public static float paceIntoSpeed(Pace pace) {
		System.out.print("Enter your pace (min/km): ");
		int seconds = pace.parse();
		return (float) (Math.round(3600/(float)seconds * 10.0) / 10.0);
	}

	public static void distancePace(Pace pace) {
		int[] result = new int[3];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Simple multiplier for target time.");

		System.out.print("Enter your pace (min/km): ");
		int seconds = pace.parse();
		System.out.println("3 km / 5 km / 10 km / 15 km / 21,1 km / 42,2 km");
		System.out.print("And enter your distance (km): ");
		float distance = scanner.nextFloat();

		seconds *= distance;
		if (seconds > 3600) {
			result[0] = seconds / 3600;
			result[1] = (seconds % 3600) / 60;
			result[2] = seconds % 60;
		} else if (seconds > 60) {
			result[1] = seconds / 60;
			result[2] = seconds % 60;
		} else if (seconds < 60) {
			result[2] = seconds;
		}

		System.out.print("Your time is ");
		for (short i = 0; i < result.length; i++) {
			if (i != result.length - 1) System.out.print(result[i] + ":");
			else System.out.print(result[i]);
		}

	}

	public static void pulseZonesJackDanielsAlgorithm() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter your age: ");
		short age = scanner.nextShort();
		float maxHR = 212 - age;
		System.out.println("Zone 1 (Easy)............... " + (int)(maxHR * 0.6) + " - " + (int)(maxHR * 0.69));
		System.out.println("Zone 2 (Comfort, fast)...... " + (int)(maxHR * 0.7) + " - " + (int)(maxHR * 0.79));
		System.out.println("Zone 3 (Fast)............... " + (int)(maxHR * 0.8) + " - " + (int)(maxHR * 0.89));
		System.out.println("Zone 4 (Very fast).......... " + (int)(maxHR * 0.9) + " - " + (int)(maxHR * 0.94));
		System.out.println("Zone 5 (Maximum)............ " + (int)(maxHR * 0.95) + " - " + (int)(maxHR));
	}

	void greeter(){
		System.out.println("###   Welcome to RunnerHelper!   ###");
		System.out.println(" ##       Version 0.0.1a	     ##");
		System.out.println("  #______________________________#");
		System.out.println("  #    Choose option below by    #");
		System.out.println("  #       entering number	     #");
		System.out.println("  #______________________________#");
		System.out.println();
		System.out.println();
	}


	public static void main(String[] args) {
		Pace pace = new Pace();
		Scanner scanner = new Scanner(System.in);
		while (true){
		  	System.out.println("\n____________________________________");
			System.out.println("(1) Pace into speed");
			System.out.println("(2) Time by pace and distance");
			System.out.println("(3) Pulse zones by Daniels algorithm");
			System.out.println("(4) EXIT");

			System.out.print("$ ");
			byte option = scanner.nextByte();
			switch (option){
				case 1:
					System.out.print(paceIntoSpeed(pace) + " km/h\n");
					break;
				case 2:
					distancePace(pace);
					break;
				case 3:
					pulseZonesJackDanielsAlgorithm();
					break;
				case 4:
					return;
				default:
					System.out.println("Wrong option!");
					break;
			}

		}
	}
}
